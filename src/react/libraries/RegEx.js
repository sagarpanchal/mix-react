const regEx = {
  email: new RegExp(
    "^[A-Za-z0-9](([_.-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([.-]?[a-zA-Z0-9]+)*).([A-Za-z]{2,})$"
  ),
  name: new RegExp("^[A-Za-z0-9]{2,}$"),
  phone: new RegExp("^[1-9]{1}[0-9]{9}$")
};

export default regEx;
