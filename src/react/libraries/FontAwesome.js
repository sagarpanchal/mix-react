import { library } from "@fortawesome/fontawesome-svg-core";
import { faHandPeace } from "@fortawesome/free-solid-svg-icons/faHandPeace";
import { faExclamationCircle } from "@fortawesome/free-solid-svg-icons/faExclamationCircle";

library.add(faHandPeace, faExclamationCircle);
