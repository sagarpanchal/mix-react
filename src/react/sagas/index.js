import { all, delay, put, takeEvery } from "redux-saga/effects";

function* sayHelloAsync(action) {
  yield delay(1000);
  let text = "Hello, " + action.data + "!";
  yield put({ type: "SAY_HELLO", data: text });
}

function* watchSayHello() {
  yield takeEvery("SAY_HELLO_ASYNC", sayHelloAsync);
}

export default function* rootSaga() {
  yield all([watchSayHello()]);
}
