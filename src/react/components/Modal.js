import React from "react";
import PropTypes from "prop-types";
import OModal from "reactstrap/es/Modal";
import ModalHeader from "reactstrap/es/ModalHeader";
import ModalBody from "reactstrap/es/ModalBody";
import ModalFooter from "reactstrap/es/ModalFooter";

const Modal = ({ isOpen, toggle, size, className, header, body, footer }) => {
  !size && (size = "md");
  return (
    <OModal isOpen={isOpen} toggle={toggle} size={size} className={className}>
      {header && <ModalHeader toggle={toggle}>{header}</ModalHeader>}
      {body && <ModalBody>{body}</ModalBody>}
      {footer && <ModalFooter>{footer}</ModalFooter>}
    </OModal>
  );
};

const children = PropTypes.oneOfType([
  PropTypes.arrayOf(PropTypes.node),
  PropTypes.node
]);

Modal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  size: PropTypes.string,
  className: PropTypes.string,
  header: children,
  body: children,
  footer: children
};

export default Modal;
