import React from "react";
import PropTypes from "prop-types";
import { Button } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { connect } from "react-redux";
import { sayHelloAsync } from "../actions";

const Hello = ({ text, sayHello }) => {
  return (
    <div className="container-fluid my-3">
      <Button size="sm" onClick={() => sayHello("World")}>
        Say Hello after a second
      </Button>
      <hr />
      <h5>
        {text + " "}
        {text !== "..." ? <FontAwesomeIcon icon="hand-peace" /> : null}
      </h5>
    </div>
  );
};

Hello.propTypes = {
  text: PropTypes.string,
  sayHello: PropTypes.func
};

export default connect(
  state => {
    return { text: state.reducer.text };
  },
  dispatch => {
    return {
      sayHello: text => {
        dispatch(sayHelloAsync(text));
      }
    };
  }
)(Hello);
