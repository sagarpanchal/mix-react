import React from "react";
import PropTypes from "prop-types";

const Spinner = ({ noBackdrop, show }) => {
  return show ? (
    <React.Fragment>
      {!noBackdrop && (
        <div className="custom-backdrop show" style={{ zIndex: 1055 }} />
      )}
      <div
        className="loading-spinner spinner-border"
        role="status"
        style={{ zIndex: 1054 }}
      >
        <span className="sr-only">Loading...</span>
      </div>
    </React.Fragment>
  ) : (
    ""
  );
};

Spinner.propTypes = {
  noBackdrop: PropTypes.bool,
  show: PropTypes.bool
};

export default Spinner;
