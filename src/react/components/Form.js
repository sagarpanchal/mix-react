import React, { Component, Fragment } from "react";
import Button from "reactstrap/es/Button";
import RenderFields from "./Form/Fields/RenderFields";
import Spinner from "./Spinner";
import { Form as FormikForm, Formik } from "formik";
import { validateFields } from "./Form/Validate";
import regEx from "../libraries/RegEx";

class Form extends Component {
  validateForm = values => {
    const { name, phone, email, address } = values;
    return validateFields(
      { name, phone, email, address },
      { name: regEx.name, email: regEx.email, phone: regEx.phone },
      { email: "Invalid address", phone: "Must be =10 numbers" }
    );
  };

  handleSubmit = async (values, formikBag) => {
    console.log(values, "\n", formikBag);
    // const { setSubmitting } = formikBag;
    // setSubmitting(true);
    // await axios.post(values).then(response => {
    //   const { status, data } = response;
    //   if (status === 200 && data.hasOwnProperty("name")) {
    //   } else if (
    //     data.hasOwnProperty("code") &&
    //     data.hasOwnProperty("message")
    //   ) {
    //   } else {
    //   }
    //   setSubmitting(false);
    // });
  };

  renderForm = props => {
    const {
      dirty,
      errors,
      handleBlur,
      handleChange,
      isSubmitting,
      isValid,
      touched,
      values
    } = props;

    const formikProps = {
      handleBlur,
      handleChange,
      values,
      touched,
      errors
    };

    const fields = [
      { name: "name" },
      { name: "email" },
      { name: "phone" },
      { name: "address", type: "textarea" }
    ];

    return (
      <Fragment>
        <Spinner show={isSubmitting} />
        <FormikForm className="w-100 float-left pt-2">
          <RenderFields fields={fields} formikProps={formikProps} />
          <Button
            size="sm"
            color="success"
            className="float-right"
            type="submit"
            disabled={!dirty || !isValid || isSubmitting}
          >
            Submit
          </Button>
          <Button
            size="sm"
            color="danger"
            className="float-left mr-2"
            type="reset"
            disabled={!dirty || isSubmitting}
          >
            Cancel
          </Button>
        </FormikForm>
      </Fragment>
    );
  };

  render() {
    const initVals = { name: "", email: "", phone: "", address: "" };

    return (
      <Fragment>
        <Formik
          initialValues={initVals}
          isInitialValid={false}
          validate={this.validateForm}
          onSubmit={this.handleSubmit}
          render={this.renderForm}
        />
      </Fragment>
    );
  }
}

export default Form;
