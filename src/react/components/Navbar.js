import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import NavBar from "reactstrap/es/Navbar";
import NavbarToggler from "reactstrap/es/NavbarToggler";
import Collapse from "reactstrap/es/Collapse";

class Navbar extends Component {
  static children = PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]);

  static propTypes = {
    color: PropTypes.string,
    brand: this.children,
    navs: this.children,
    offCanvas: PropTypes.bool
  };

  state = {
    isOpen: false
  };

  toggle = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  };

  render() {
    const { color, brand, navs, offCanvas } = this.props;
    let collapseClasses, backdropClasses;
    if (offCanvas) {
      backdropClasses = ["custom-backdrop", "d-sm-none"];
      collapseClasses = [
        "navbar-collapse",
        "offcanvas-collapse",
        "bg-" + color
      ];
      this.state.isOpen &&
        (collapseClasses.push("open") && backdropClasses.push("show"));
    }

    return (
      <Fragment>
        {offCanvas && <div className={backdropClasses.join(" ")} />}
        <NavBar color={color} dark expand="sm" fixed="top">
          {brand}
          <NavbarToggler onClick={this.toggle} />
          {offCanvas ? (
            <div className={collapseClasses.join(" ")}>{navs}</div>
          ) : (
            <Collapse isOpen={this.state.isOpen} navbar>
              {navs}
            </Collapse>
          )}
        </NavBar>
      </Fragment>
    );
  }
}

export default Navbar;
