import React, { Fragment } from "react";
import PropTypes from "prop-types";

const layout = ({ children }) => {
  return (
    <Fragment>
      <div className="container-fluid">{children}</div>
    </Fragment>
  );
};

layout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ])
};

export default layout;
