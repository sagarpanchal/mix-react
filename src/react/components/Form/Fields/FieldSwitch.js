import React, { Component } from "react";
import PropTypes from "prop-types";
import { Field } from "formik";
import FieldError from "./FieldError";
import FieldLabel from "./FieldLabel";
import FormGroup from "reactstrap/es/FormGroup";
import startCase from "lodash/startCase";

export default class RenderSwitch extends Component {
  static propTypes = {
    fieldName: PropTypes.string.isRequired,
    fieldTitle: PropTypes.string,
    formikProps: PropTypes.object.isRequired,
    required: PropTypes.bool,
    classes: PropTypes.string
  };

  render() {
    let { fieldName, fieldTitle, formikProps, ...otherProps } = this.props;
    let { handleBlur, handleChange, values, touched, errors } = formikProps;
    !fieldTitle && (fieldTitle = startCase(fieldName));

    return (
      <FormGroup className="custom-control custom-switch">
        <Field
          component="input"
          type="checkbox"
          className="custom-control-input"
          name={fieldName}
          id={fieldName}
          onChange={handleChange}
          onBlur={handleBlur}
          checked={values[fieldName]}
          {...otherProps}
        />
        <FieldLabel
          htmlFor={fieldName}
          text={fieldTitle}
          className="custom-control-label"
        />
        <FieldError touched={touched[fieldName]} error={errors[fieldName]} />
      </FormGroup>
    );
  }
}
