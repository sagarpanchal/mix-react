import React, { Component } from "react";
import PropTypes from "prop-types";
import { Field } from "formik";
import FieldError from "./FieldError";
import FieldLabel from "./FieldLabel";
import FormGroup from "reactstrap/es/FormGroup";
import startCase from "lodash/startCase";

export default class FieldInput extends Component {
  static propTypes = {
    fieldName: PropTypes.string.isRequired,
    fieldTitle: PropTypes.string,
    type: PropTypes.string,
    formikProps: PropTypes.object.isRequired,
    required: PropTypes.bool,
    classes: PropTypes.string
  };

  render() {
    let {
      fieldName,
      fieldTitle,
      type,
      formikProps,
      classes,
      ...otherProps
    } = this.props;
    let { handleBlur, handleChange, values, touched, errors } = formikProps;
    !fieldTitle && (fieldTitle = startCase(fieldName));
    !type && (type = "text");
    !classes && (classes = "form-control");
    touched[fieldName] && errors[fieldName] && (classes += " is-invalid");

    return (
      <FormGroup className="has-float-label">
        <Field
          className={classes}
          type={type}
          name={fieldName}
          onChange={handleChange}
          onBlur={handleBlur}
          value={values[fieldName]}
          placeholder=""
          {...otherProps}
        />
        <FieldLabel htmlFor={fieldName} text={fieldTitle} />
        <FieldError touched={touched[fieldName]} error={errors[fieldName]} />
      </FormGroup>
    );
  }
}
