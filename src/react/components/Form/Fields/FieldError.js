import React, { Component } from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default class FieldError extends Component {
  static propTypes = {
    touched: PropTypes.bool,
    error: PropTypes.string
  };

  render() {
    const { touched, error } = this.props;
    return touched && error ? (
      <div className="feedback">
        <small className="text-danger">
          <FontAwesomeIcon icon="exclamation-circle" className="mr-1" />
          {error}
        </small>
      </div>
    ) : null;
  }
}
