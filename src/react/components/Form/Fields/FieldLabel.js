import React, { Component } from "react";
import PropTypes from "prop-types";
import Label from "reactstrap/es/Label";

export default class FieldLabel extends Component {
  static propTypes = {
    htmlFor: PropTypes.string,
    text: PropTypes.string
  };

  focusInput = ({ target }) => {
    let field = document.getElementsByName(target.htmlFor)[0];
    field !== undefined && field.focus();
  };

  render() {
    const { htmlFor, text, ...rest } = this.props;
    return (
      <Label htmlFor={htmlFor} onClick={this.focusInput} {...rest}>
        {text}
      </Label>
    );
  }
}
