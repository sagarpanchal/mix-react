import React, { Component } from "react";
import PropTypes from "prop-types";
import FieldInput from "./FieldInput";
import FieldTextarea from "./FieldTextarea";
import FieldSelect from "./FieldSelect";

export default class RenderFields extends Component {
  static propTypes = {
    fields: PropTypes.array,
    formikProps: PropTypes.object
  };

  render() {
    const { fields, formikProps } = this.props;
    return fields.map(field => {
      let { name, type, options, fieldsList } = field;
      !options && type === "select" && (options = [{ value: "", text: "" }]);

      if (!type) {
        name !== "email" ? (type = "text") : (type = "email");
      }

      switch (type) {
        case "text":
        case "email":
        case "date":
        case "password":
          return (
            <FieldInput
              key={name}
              fieldName={name}
              type={type}
              formikProps={formikProps}
            />
          );

        case "group":
          return (
            <div className="row m-0">
              <div className="col-12 col-sm-6 p-0 col-right">
                <RenderFields
                  fields={[fieldsList[0]]}
                  formikProps={formikProps}
                />
              </div>
              <div className="col-12 col-sm-6 p-0 col-left">
                <RenderFields
                  fields={[fieldsList[1]]}
                  formikProps={formikProps}
                />
              </div>
            </div>
          );

        case "textarea":
          return (
            <FieldTextarea
              key={name}
              fieldName={name}
              formikProps={formikProps}
            />
          );

        case "select":
          return (
            <FieldSelect
              key={name}
              fieldName={name}
              options={options}
              formikProps={formikProps}
            />
          );
      }
    });
  }
}
