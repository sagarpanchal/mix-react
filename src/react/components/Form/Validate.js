import startCase from "lodash/startCase";

export const validateFields = (values, regExp, customError) => {
  let fields = Object.keys(values);
  let errors = {};
  fields.forEach(field => {
    !values[field] && (errors[field] = startCase(field) + " is required");
    if (!errors[field] && regExp[field]) {
      if (!regExp[field].test(values[field])) {
        errors[field] = customError.hasOwnProperty(field)
          ? customError[field]
          : "Invalid value";
      }
    }
  });
  return errors;
};
