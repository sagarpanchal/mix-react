import React, { Component } from "react";
import Hello from "./components/Hello";

export default class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Hello />
      </React.Fragment>
    );
  }
}
