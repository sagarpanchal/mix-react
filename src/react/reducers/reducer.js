const initState = {
  text: "..."
};

const reducer = (state = initState, action) => {
  switch (action.type) {
    case "SAY_HELLO":
      return { ...state, text: action.data };

    default:
      return state;
  }
};

export default reducer;
