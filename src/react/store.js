import { combineReducers, createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension/developmentOnly";
// import { reducer as formReducer } from "redux-form";
import reducers from "./reducers";
import middleware from "./middleware";

const composeEnhancers = composeWithDevTools({
  trace: true
});

export const store = createStore(
  combineReducers({
    ...reducers
    // form: formReducer
  }),
  composeEnhancers(applyMiddleware(...middleware))
);
