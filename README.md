# Setup Instructions

### Clone this repo in current directory

**Create and switch to a new empty directory and run following command**

```
git clone https://gitlab.com/sagarpanchal/mix-react.git .
```

**Install all dependencies**

```
npm i
```

**Run the project**

```
npm run watch   # live updates while testing
npm run dev     # development build
npm run prod    # production build
```

### OPTIONAL

**ESLint plugins for vscode**

```
npm i -g eslint eslint-plugin-react babel-eslint
```

**Remove existing git repo using powershell on windows**

```
Remove-Item -Recurse -Force .\.git\
```
